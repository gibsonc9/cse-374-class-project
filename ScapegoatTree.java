import java.util.ArrayList;

public class ScapegoatTree {
class Node {
  // Node Properties
  int value;
  Node leftChild;
  Node rightChild;
  Node parentNode;
  
  // Node Constructor
  public Node(int value) {
    this.value = value;
    this.leftChild = null;
    this.rightChild = null;
    this.parentNode = null;
  }
}

// Scapegoat Tree Properties
Node root;
int size;

// Scapegoat Tree Constructor
public ScapegoatTree() {
  this.root = null;
  this.size = 0;
}
void insert(int value) {
  Node node = new Node(value);
  if (this.root == null) {
    this.root = node;
  }
  else {
    Node currentNode = this.root;
    Node newParent = new Node(-1);
    while (currentNode != null) {
      newParent = currentNode;
      if (node.value < currentNode.value) {
        currentNode = currentNode.leftChild;
      } else {
        currentNode = currentNode.rightChild;
      }
    }
    node.parentNode = newParent;
    if (node.value < node.parentNode.value) {
      node.parentNode.leftChild = node;
    } else {
      node.parentNode.rightChild = node;
    }
    this.size += 1;
    Node scapegoat = this.findScapegoat(node);
    if (scapegoat != null) {
      Node tempNode = this.rebalance(scapegoat);
      scapegoat.leftChild = tempNode.leftChild;
      scapegoat.rightChild = tempNode.rightChild;
      scapegoat.value = tempNode.value;
      scapegoat.leftChild.parentNode = scapegoat;
      scapegoat.rightChild.parentNode = scapegoat;
    }
   }
}
Node findScapegoat(Node node) {
  if (node == this.root) {
    return null;
  }
  while (this.nodeBalanced(node) == true) {
    if (node == this.root) {
      return null;
    }
    node = node.parentNode;
  }
  return node;
}

boolean nodeBalanced(Node node) {
  if (Math.abs(this.subtreeSize(node.leftChild) - this.subtreeSize(node.rightChild)) <= 1) {
    return true;
  } else {
    return false;
  }
}

int subtreeSize(Node node) {
  if (node == null) {
    return 0;
  } else {
    return 1 + this.subtreeSize(node.leftChild) + this.subtreeSize(node.rightChild);
  }
}

Node rebalance(Node root) {
  ArrayList<Node> nodes = new ArrayList<Node>();
  flatten(root, nodes);
  return buildTree(nodes, 0, nodes.size() - 1);
}

void flatten(Node node, ArrayList<Node> nodes) {
  if (node != null) {
    flatten(node.leftChild, nodes);
    nodes.add(node);
    flatten(node.rightChild, nodes); 
  }
}

Node buildTree(ArrayList<Node> nodes, int start, int end) {
  if (start > end) {
    return null;
  }
  int middle = (start + (end - start)) / 2; 
  Node node = new Node(nodes.get(middle).value);
  node.leftChild = buildTree(nodes, start, middle - 1);
  node.rightChild = buildTree(nodes, middle + 1, end);
  return node;
}

void delete(int value) {
  if (this.root != null) {
  Node node = new Node(-1);
  node = this.root;
  while (node != null && node.value != value) {
    if (node.value > value) {
      node = node.rightChild;
    } else if (node.value < value) {
      node = node.leftChild;
    }
  }
  if (node != null) {
    if (node.leftChild == null && node.rightChild == null) {
      if (node.parentNode.leftChild == node) {
        node.parentNode.leftChild = null;
      } else if (node.parentNode.rightChild == node) {
        node.parentNode.rightChild = null;
      }
      node = null;
    } else if (node.leftChild == null && node.rightChild != null) {
      node.value = node.rightChild.value;
      node.rightChild = null;
    } else if (node.leftChild != null && node.rightChild == null) {
      node.value = node.leftChild.value;
      node.leftChild = null;
    } else {
      
    }
  }
  
}
}
}
